module Swat
  #
  # Test command
  #
  class Command < BaseCommand
    def prepare(context)
      fail "I need at least 1 argument" if @args.empty?
      context[:prepared] = "done"
      "preparation is fine so far"
    end

    def pre_check(context)
      fail "something is not right" unless @args[0].to_sym == :success
      context[:checks] = "done"
      "all is gut"
    end

    def execute(context)
      fail "execution failed!" unless @args[1].to_sym == :success
      "Context so far is #{context}"
    end
  end
end
