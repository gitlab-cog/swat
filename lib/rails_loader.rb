require "open3"

module Swat
  #
  # Loads Rails command runner
  #
  # Loads RAILS_RUNNER_COMMAND from the environment to define
  #
  # Sample script:
  #
  # SCRIPTS_LOCAL_PATH=path_to/scripts bundle exec rails runner
  # path_to/scripts/lib/swat_run.rb execute test success success 2&>/dev/null
  #
  # Can read the runner command and the working dir from environment variables
  # such as:
  #   RAILS_RUNNER_COMMAND
  #   RAILS_WORKING_DIR
  #
  class RailsLoader
    def initialize(command = ENV["RAILS_RUNNER_COMMAND"] || "",
                   working_dir = ENV["RAILS_WORKING_DIR"] || Dir.getwd)
      fail "Invalid RAILS_RUNNER_COMMAND, please provide a rails command" if command.nil? || command.empty?
      fail "Invalid RAILS_WORKING_DIR, '#{working_dir}' does not exists" unless Dir.exist?(working_dir)
      @rails_command = command
      @rails_working_dir = working_dir
    end

    def run(args)
      command = "#{@rails_command} #{args}"
      env = {
        "HOME" => ENV["HOME"],
        "PATH" => ENV["PATH"],
        "SCRIPTS_REMOTE_URL" => ENV["SCRIPTS_REMOTE_URL"],
        "SCRIPTS_LOCAL_PATH" => ENV["SCRIPTS_LOCAL_PATH"]
      }
      Open3.popen3(env, command, unsetenv_others: true, chdir: @rails_working_dir) do |_, stdout, stderr, wait_thr|
        if wait_thr.value != 0
          fail "Command #{args} failed with err: '#{stderr.read.strip}' " \
               "out: '#{stdout.read.strip}'"
        end
        JSON.parse(stdout.read.strip)
      end
    end
  end
end
