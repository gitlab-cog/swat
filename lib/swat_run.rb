# Usage: through rails execute the following command:
#   bundle exec rails runner <path to>/marvin_run.rb <command> [arguments]
#
# Such as:
#   - <path to>/marvin.rb points to the place where this file is located
#   - command is the name of a file existing inside the scripts folder of this repo
#   - arguments are the required arguments for the given command
#
# Implementing new commands:
#   - Create a file in the scripts folder such as scripts/<command>.rb
#   - In this file create a Swat::Command class that inheriths Swat::BaseCommand
#   - It is mandatory to override pre_check and execute, these methods should either execute or fail with an exception
#   - Optionally the method `validate` can be implemented to validate the received arguments before execution

$LOAD_PATH.unshift(File.dirname(__FILE__))

require "json"
require_relative "swat"

puts Swat.run.to_json
