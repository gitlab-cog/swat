require "fileutils"

module Swat
  #
  # Git handler
  #
  class Git
    attr_accessor :source, :target

    def initialize(source = ENV["SCRIPTS_REMOTE_URL"], target = ENV["SCRIPTS_LOCAL_PATH"])
      @source = source
      @target = target
    end

    def wipe
      FileUtils.rm_rf(@target) if File.writable?(@target)
    end

    def update
      if Dir.exist?(@target)
        pull
      else
        clone
      end
    end

    def valid?
      return false unless File.exist?(@target)
      Dir.chdir(@target) {
        `git config --get remote.origin.url`.strip == @source
      }
    end

    private

    def clone
      repo_path = Pathname.new(@target)
      parent_folder = repo_path.parent
      repo_name = repo_path.basename
      fail "Invalid target path #{parent_folder}" unless File.writable?(parent_folder)
      Dir.chdir(parent_folder) do
        `git clone #{@source} #{repo_name} 2> /dev/null`
      end
      { action: "clone", head: current_commit }
    end

    def pull
      fail "Invalid target repo #{@target}" unless valid?
      Dir.chdir(@target) do
        `git pull origin 2> /dev/null`
      end
      { action: "pull", head: current_commit }
    end

    def current_commit
      fail "Invalid target repo #{@target}" unless valid?
      Dir.chdir(@target) do
        `git log --oneline -1 HEAD`.strip
      end
    end
  end
end
