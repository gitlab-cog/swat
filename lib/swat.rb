require "pathname"
#
# Swat module, where all the stuff lives
#
module Swat
  #
  # Defines the basic behavior of a command
  #
  # Inherith to reuse all this logic, only pre-check and execute are mandatory methods
  # the rest can be as is
  #
  # Use the context object to send state from one stage to the next
  class BaseCommand
    def initialize(args)
      @args = args
    end

    def prepare(_context)
      # validate arguments here
    end

    def pre_check(_context)
      fail "PreChecks are not defined"
    end

    def execute(_context)
      fail "Execution is not defined"
    end
  end

  #
  # Represents the whole execution pipeline of a command
  #
  # Returns a hashmap that contains the following keys
  #   execution_mode: self descriptive
  #   prepare, precheck, execute: the different stages of execution
  #     each stage will include a hash with:
  #       succesful: boolean, indicating if it was successful or not
  #       output: the stdout in case the stage was successfull, stderr otherwise
  class CommandExecution
    def initialize(command, execution_mode = "dryrun")
      @command = command
      @stages = stages(execution_mode)
      @result = { execution_mode: execution_mode }
      @context = {}
    end

    def run
      @stages.each do |stage|
        execute_stage(stage)
        break unless @result[stage][:successful]
      end
      @result
    end

    def execute_stage(stage)
      @result[stage] = { successful: true, output: @command.public_send(stage, @context) }
    rescue => e
      @result[stage] = { successful: false, output: e.to_s }
    end

    def stages(execution_mode)
      case execution_mode
      when "dryrun"
        %i(prepare pre_check)
      when "execute"
        %i(prepare pre_check execute)
      else
        fail "Invalid execution mode '#{execution_mode}'"
      end
    end
  end

  #
  # Parameters parsing helper class
  #
  # Assumes the first parameter to be the execution mode (dryrun or execute)
  # Assumes the second parameter to be the script name
  # Captures the rest as arguments that are piped in to the script
  class Parameters
    attr_accessor :execution_mode, :command, :args

    def initialize(args = ARGV.clone)
      @args = args
      @execution_mode = @args.shift
      @command = @args.shift
      fail "Execution mode is mandatory" if @execution_mode.nil?
      fail "No command was specified" if @command.nil?
    end

    def to_s
      "#{@execution_mode} #{@command} #{@args.inspect}"
    end
  end

  #
  # An executable script
  #
  # Loads the script from file system using ruby's `require` command, then creates a
  # ::Swat::Command and then calls `run` on it
  #
  # If a Swat::Command object cannot be found (NameError) the command is considered bogus.
  class Script
    def initialize(parameters, scripts_path = ENV["SCRIPTS_LOCAL_PATH"])
      @parameters = parameters
      @scripts_path = Pathname.new(scripts_path || "scripts")
    end

    def run
      CommandExecution.new(create_command, @parameters.execution_mode).run
    end

    private

    def command_file
      @command_file ||= @scripts_path.join("#{@parameters.command}.rb")
    end

    def create_command
      fail "Could not find command #{@parameters.command} in #{command_file.expand_path}" unless command_file.file?
      require command_file.expand_path
      ::Swat::Command.new(@parameters.args)
    rescue NameError
      raise "#{@parameters.command} does not define a Swat::Command object"
    end
  end

  def self.run
    Script.new(Parameters.new).run
  end
end
