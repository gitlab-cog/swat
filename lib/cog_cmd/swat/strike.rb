require "cog"
require "swat"
require "rails_loader"

module CogCmd
  module Swat
    #
    # Cog Command that loads and executes the given script
    #
    class Strike < Cog::Command
      def run_command
        rails = ::Swat::RailsLoader.new
        response.template = "execution_result"
        response.content = rails.run("execute #{request.args.join(' ')}")
      end
    end
  end
end
