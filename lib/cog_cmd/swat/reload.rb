require "cog"
require "json"
require "swat_git"

module CogCmd
  module Swat
    #
    # Cog Command that [re]loads a local git repo for scripts
    #
    class Reload < Cog::Command
      def run_command
        git = ::Swat::Git.new
        git.wipe if wipe?
        result = { source: git.source,
                   target: git.target,
                   wiped: wipe? }.merge(git.update)
        response.content = result
      end

      def wipe?
        request.options["wipe"] == true || request.options["wipe"] == "true"
      end
    end
  end
end
