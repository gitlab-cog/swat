require_relative "spec_helper"
require "yaml"

describe "Configuration file" do
  it "can be parsed as correct yaml" do
    expect(YAML.parse("config.yaml")).not_to be_nil
  end
end
