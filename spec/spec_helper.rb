require "simplecov"

SimpleCov.start do
  add_filter "vendor"
end

require "rspec"
require "swat"
require "tmpdir"
require "fileutils"

class GitSpecHelper
  attr_accessor :source_repo, :target_dir

  def initialize
    @source_repo = Dir.mktmpdir
    @target_dir = Dir.mktmpdir
    Dir.chdir(@source_repo) do
      `git init`
      `echo "hi" > readme.md`
      `git add readme.md`
      `git commit -m "initial commit"`
    end
    clean_target
  end

  def destroy
    FileUtils.rm_rf(@source_repo)
    FileUtils.rm_rf(@target_dir) if File.exist?(@target_dir)
  end

  def clean_target
    FileUtils.rm_rf(@target_dir)
  end
end

def with_environment(args: [])
  ENV["COG_ARGC"] = args.length.times { |n| ENV["COG_ARGV_#{n}"] = args[n] }.to_s
  yield
ensure
  ENV.delete("COG_ARGC")
  args.length.times { |n| ENV.delete("COG_ARGV_#{n}") }
end
