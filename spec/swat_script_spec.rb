require_relative "spec_helper"

describe Swat::Script do
  context "with valid parameters" do
    let(:params) {
      Swat::Parameters.new(%w(dryrun test arg1 arg2))
    }

    it "can be created" do
      script = Swat::Script.new(params)
      expect(script).not_to be_nil
    end

    it "finds the test script" do
      script = Swat::Script.new(params)
      script.run
    end

    it "fils to find a valid script with a descriptive message" do
      script = Swat::Script.new(params, "scr")
      expect { script.run }.to raise_error(/Could not find command test in /)
    end
  end
end
